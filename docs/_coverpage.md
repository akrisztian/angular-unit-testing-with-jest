<!-- _coverpage.md -->

![logo](assets/angular_logo.png ':size=100px')
![logo](assets/jest_logo.png ':size=100px')

# Angular Unit Testing with Jest

> A practical guide to get you started on testing your angular apps


[GitLab](https://gitlab.com/akrisztian/angular-unit-testing-with-jest)
[Get Started](pages/getting_started.md)
