## Disclaimer

This documentation is written in my spare time with the purpose of helping my friends and colleagues.

The focus on Angular is given by the lack of comprehensive documentation on the subject: many tutorials only present how to set up Jest to work with Angular, but do not go in depth as to how exactly tests are written and run.

Currently, the scope of this documentation is to guide the reader through the setup process, and then provide descriptions and useful examples about testing the **interfaces of angular components** (So testing only the _.ts_ files).

If you happen to find anything that is incorrect or could be done better, please feel free to [contribute!](https://gitlab.com/akrisztian/angular-unit-testing-with-jest)

## Setup

